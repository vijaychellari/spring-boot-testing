package com.wipro.LibrarySystemJunit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.LibrarySystemJunit.entity.LibraryBook;

public interface LibrarySystemRepo extends JpaRepository<LibraryBook, Long>{

}
